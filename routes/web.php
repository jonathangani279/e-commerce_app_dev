<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\ViewController;
use App\Http\Controllers\SubViewController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\LogoutController;
use App\Http\Controllers\SocialiteController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\GameController;
use App\Http\Controllers\LibraryController;
use App\Http\Controllers\PaymentController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//ROUTE VIEW
Route::get('/', [ViewController::class, 'landing']);
Route::get('/register', [ViewController::class, 'register']);
Route::get('/login', [ViewController::class, 'login']);
Route::get('/logout', [LogoutController::class, 'index']);

//ROUTE VIEW - ADMIN
Route::get('/admin', [ViewController::class, 'admin']);

//ROUTE VIEW - CUSTOMER
Route::get('/products', [ViewController::class, 'products']);
Route::get('/products/{url_path}', [ViewController::class, 'products_detail']);
Route::get('/libraries', [ViewController::class, 'libraries']);
Route::get('/profile', [ViewController::class, 'profile']);

//ROUTE VIEW - PUBLISHER
Route::get('/publisher', [ViewController::class, 'publisher']);

//Route VIEW - TOPUP
Route::get('/topup', [ViewController::class, 'topup']);

//ROUTE VIEW - CART
Route::get('/cart', [ViewController::class,'cart']);

//ROUTE SUB-VIEW
Route::group(['prefix' => 'sub-view'], function () {
    Route::get('/products/list', [SubViewController::class, 'products_list']);
    Route::get('/libraries/list', [SubViewController::class, 'libraries_list']);
    Route::get('/libraries/detail', [SubViewController::class, 'libraries_detail']);
    Route::get('/profile/dashboard', [SubViewController::class, 'profile_dashboard']);
    Route::get('/publisher/dashboard', [SubViewController::class, 'publisher_dashboard']);
});

//ROUTE SOCIALITE
Route::get('/auth/google', [SocialiteController::class, 'redirectToGoogle']);
Route::get('/auth/google/callback', [SocialiteController::class, 'handleGoogleCallback']);

//ROUTE API
Route::group(['prefix' => 'api'], function () {
    Route::post('/register', [RegisterController::class, 'index']);
    Route::post('/login', [LoginController::class, 'index']);

    //USERS
    Route::group(['prefix' => 'users'], function () {
        Route::put('/{id}', [UserController::class, 'update']);
    });

    //GAMES
    Route::group(['prefix' => 'games'], function () {
        Route::get('/', [GameController::class, 'index']);
        Route::get('/list', [GameController::class, 'list']);
        Route::get('/{id}', [GameController::class, 'show']);
        Route::post('/addGame', [GameController::class, 'addGame']);
    });

    //LIBRARIES
    Route::group(['prefix' => 'libraries'], function () {
        Route::get('/', [LibraryController::class, 'index']);
        Route::get('/list', [LibraryController::class, 'list']);
        Route::get('/{id}', [LibraryController::class, 'show']);
    });

    //CARTS
    Route::group(['prefix' => 'carts'], function () {
        // Route::post('/addToCart/{id}',[CartController::class,'addToCart']);
        Route::get('/addToCart/{id}',[CartController::class,'addToCart']);

    });

    //WISHLISTS
    Route::group(['prefix' => 'wishlists'], function () {

    });

    //TRANSACTIONS
    Route::group(['prefix' => 'transactions'], function () {

    });

    //ADMIN
    Route::group(['prefix' => 'admin'], function () {
        Route::get('/dashboard', [AdminController::class, 'dashboard']);
        Route::get('/mastergame', [AdminController::class, 'masterGame']);
        Route::post('/toggleStatusGame', [AdminController::class, 'toggleStatusGame']);
        Route::get('/getPublishers', [AdminController::class, 'getPublishers']);
        Route::put('/toggleStatusPublisher', [AdminController::class, 'toggleStatusPublisher']);
    });

    //PAYMENT
    Route::group(['prefix' => 'payment'], function () {
        Route::post('/getSnapToken',[PaymentController::class,'getSnapToken']);
    });
});

