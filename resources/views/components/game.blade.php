<div class="column {{ isset($css_class) ? $css_class : '' }}">
    <figure class="imghvr-flip-diag-1 box">
        <img src="https://cdn.mos.cms.futurecdn.net/6YDMcHZCvPZJ5HhZfjRQSN-320-80.jpg" class="game-box">
        <figcaption class="game-hover">
            <div class="container has-text-centered">
                <h5 class="title is-5 mb-5">
                    {{ $game->title_game }} - {{ $game->price_game }}
                </h5>
                <a href="{{ $game->url_path }}" class="button is-rounded has-text-white mb-2">
                    Go check for It! <i class="fas fa-running ml-1"></i>
                </a>
                <a href="{{('/api/carts/addToCart/'.$game->id_game)}}" class="button is-rounded has-text-white mt-2 mb-2">
                    Put on Cart <i class="fa fa-shopping-cart ml-1"></i>
                </a>
                <a href="" class="button is-rounded has-text-white mt-2">
                    Wishing for This! <i class="fa fa-star ml-1"></i>
                </a>
            </div>
        </figcaption>
    </figure>
</div>
