<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        @include('partials.assets.main-css')

        @yield('extra-css')

        @yield('title')

        @include('partials.assets.main-js')
    </head>
    <body>
        @include('partials.header')

        @yield('content')

        <a id='backTop'></a>

        @include('partials.footer')

        @include('sweet::alert')

        @yield('extra-js')
    </body>
</html>
