@extends('layouts.base')

@section('extra-css')
    <style></style>
@endsection

@section('title')
    <title>Home</title>
@endsection

@section('content')
    <main>
        <section>
            <div class="hero is-fullheight has-background">
                <img src="{{ asset('/medias/images/heroes/afterparty.jpg') }}" class="hero-background" alt="">
                <div class="hero-body">
                    <div class="container has-text-centered">
                        <a href="" class="button is-rounded">
                            <h5 class="title is-5 has-text-white-ter">PLAY NOW</h5>
                        </a>
                    </div>
                </div>
            </div>
        </section>

        <section>
            <div class="mt-5 pt-5 mb-5 pb-5">
                <div class="container">
                    <h3 class="title is-3 has-text-centered">New Games</h3>
                    <div class="columns is-multiline">
                        @foreach ($games as $game)
                            @include('components.game', [
                                'games' => $game,
                                'css_class' => 'is-4'
                            ])
                        @endforeach
                    </div>
                    <div class="has-text-centered">
                        <a href="" class="button is-rounded has-background">
                            <h5 class="title is-5 has-text-white-ter">Show More</h5>
                        </a>
                    </div>
                </div>
            </div>
        </section>

        <section>
            <div class="mt-5 pt-5 mb-5 pb-5">
                <div class="container">
                    <h3 class="title is-3 has-text-centered">Featured Games</h3>
                    <div class="columns is-multiline">
                        @foreach ($games as $game)
                            @include('components.game', [
                                'games' => $game,
                                'css_class' => 'is-4'
                            ])
                        @endforeach
                    </div>
                    <div class="has-text-centered">
                        <a href="" class="button is-rounded has-background">
                            <h5 class="title is-5 has-text-white-ter">Show More</h5>
                        </a>
                    </div>
                </div>
            </div>
        </section>

        <section>
            <div class="mt-5">
                <div class="hero is-fullheight has-background">
                    <img src="{{ asset('/medias/images/heroes/ea-games.jpg') }}" class="hero-background" alt="">
                    <div class="hero-body">
                        <div class="container">
                            <h5 class="title is-5 has-text-white-ter">
                                Don't just look. Get the most experience game with best price.
                            </h5>
                            <a href="{{ url('/login') }}" class="button">
                                <h5 class="title is-5 has-text-white-ter">JOIN NOW</h5>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection

@section('extra-js')
    <script></script>
@endsection
