@extends('layouts.base')

@section('extra-css')
    <style>
        #publisher-dashboard {
            height: fit-content;
        }

        #publisher-dashboard h5 {
            border-bottom: 2px solid var(--theme-color);
        }

        #publisher-dashboard h5:hover {
            background-color: var(--theme-color);
            color: white;
            border-radius: 5px;
            cursor: pointer;
        }
    </style>
@endsection

@section('title')
    <title>Home</title>
@endsection

@section('content')
    <main>
        <div class="mt-5 pt-5 mb-5 pb-5">
            <div class="container">
                <div class="columns">
                    <div class="column is-3 box container mr-4 p-5" id="publisher-dashboard">
                        <h5 class="title is-5 p-2" name="about">About</h5>
                        <h5 class="title is-5 mt-4 p-2" name="my-games">My Games</h5>
                        <h5 class="title is-5 mt-4 p-2" name="add-game">Add Game</h5>
                    </div>
                    <div class="column is-three-fifths box container ml-4 p-5" id="container-spinner">
                        <div id="publisher-content"></div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection

@section('extra-js')
    <script>
        function load_dashboard(dashboard) {
            spinner.show();

            $.get('/sub-view/publisher/dashboard', { dashboard: dashboard }, function(sub_view) {
                spinner.hide();
                $('#publisher-content').html(sub_view);
            });
        }

        let spinner= new jQuerySpinner({
            parentId: 'container-spinner',
            duration: 500
        });

        load_dashboard('about');

        $('#publisher-dashboard h5').click(async function() {
            let dashboard= $(this).attr('name');

            $('#publisher-dashboard h5').each(function() {
                $(this).removeClass('aside-item-clicked');
            });
            $(this).addClass('aside-item-clicked');
            load_dashboard(dashboard);
        });

        $(document).ajaxStop(function() {
            $('input:file').change(function() {
                let input= $(this)[0];

                if (input.files && input.files[0]) {
                    let reader= new FileReader();

                    reader.onload= function(e) {
                        $('.selected-image').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            });
        });
    </script>
@endsection
