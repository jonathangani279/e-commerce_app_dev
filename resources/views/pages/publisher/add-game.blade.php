<main>
    <div class="tag has-background p-5">
        <h5 class="title is-5 has-text-white-ter">Game Information Details</h5>
    </div>
    <div class="mt-5 pt-5 mb-5 pb-5">
        <form action="{{ url('/api/games/addGame') }}" method="post">
            @csrf

            <div class="field">
                <label class="label">Game Title</label>
                <div class="control">
                    <input type="text" name="title" id="" class="input" placeholder="Your game title" required>
                </div>
            </div>

            <div class="field is-horizontal mt-5">
                <div class="field-body">
                    <div class="field">
                        <label class="label">Genres</label>
                        <input type="hidden" id="genres" name="genres" value="">
                        <select id="genre" name="genre" multiple="multiple" style="width: 75%">
                            <option value="Action">Action</option>
                            <option value="Adventure">Adventure</option>
                            <option value="Battle Royale">Battle Royale</option>
                            <option value="Board">Board</option>
                            <option value="Fighting">Fighting</option>
                            <option value="Horror">Horror</option>
                            <option value="Idle">Idle</option>
                            <option value="Open World">Open World</option>
                            <option value="Puzzle">Puzzle</option>
                            <option value="Racing">Racing</option>
                            <option value="Role-Playing">Role-Playing</option>
                            <option value="Shooting">Shooting</option>
                            <option value="Simulation">Simulation</option>
                            <option value="Strategy">Strategy</option>
                            <option value="Survival">Survival</option>
                            <option value="Sports">Sports</option>
                            <option value="Tower Defense">Tower Defense</option>
                        </select>
                    </div>

                </div>
            </div>

            <div class="field">
                <label class="label">Price</label>
                <div class="field is-horizontal">
                    <div class="field-body">
                        <div class="field is-expanded">
                            <div class="field has-addons">
                              <p class="control">
                                <a class="button is-static">
                                  IDR
                                </a>
                              </p>
                              <p class="control is-expanded">
                                <input class="input" name="price" type="tel" placeholder="Your game price">
                              </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="field">
                <label class="label">Game Description</label>
                <div class="control">
                    <textarea name="description" class="textarea" placeholder="Your game description" required></textarea>
                </div>
            </div>

            <div class="field">
                <label class="label">Game Cover</label>
                <figure class="image is-3by2 container">
                    <img class="selected-image" src="{{ asset('/medias/images/icons/no_uploaded.png') }}" alt="">
                </figure>
                <div class="file is-centered mt-4">
                    <label class="file-label">
                        <input class="file-input" type="file" name="image">
                        <span class="file-cta">
                            <span class="file-icon">
                                <i class="fas fa-upload"></i>
                            </span>
                            <span class="file-label">Upload Image</span>
                        </span>
                    </label>
                </div>
            </div>

            <div class="is-divider"></div>
            <div class="tag has-background p-5">
                <h5 class="title is-5 has-text-white-ter">Minimum Requirements</h5>
            </div>

            <div class="field mt-6">
                <label class="label">CPU</label>
                <div class="control">
                    <input type="text" name="mincpu" id="" class="input" placeholder="Minimum CPU Requirement" required>
                </div>
            </div>

            <div class="field">
                <label class="label">Graphic Processing Unit</label>
                <div class="control">
                    <input type="text" name="mingpu" id="" class="input" placeholder="Minimum GPU Requirement" required>
                </div>
            </div>

            <div class="field">
                <label class="label">RAM</label>
                <div class="control">
                    <input type="text" name="minram" id="" class="input" placeholder="Minimum RAM Requirement" required>
                </div>
            </div>

            <div class="field">
                <label class="label">Storage</label>
                <div class="control">
                    <input type="text" name="minstorage" id="" class="input" placeholder="Minimum Storage Requirement" required>
                </div>
            </div>

            <div class="field">
                <label class="label">Operating System</label>
                <div class="control">
                    <input type="text" name="minos" id="" class="input"  placeholder="Minimum OS Requirement"required>
                </div>
            </div>

            <div class="is-divider"></div>
            <div class="tag has-background p-5">
                <h5 class="title is-5 has-text-white-ter">Recommended Requirements</h5>
            </div>

            <div class="field mt-6">
                <label class="label">CPU</label>
                <div class="control">
                    <input type="text" name="reccpu" id="" class="input" placeholder="Recommended CPU Requirement" required>
                </div>
            </div>

            <div class="field">
                <label class="label">Graphic Processing Unit</label>
                <div class="control">
                    <input type="text" name="recgpu" id="" class="input" placeholder="Recommended GPU Requirement" required>
                </div>
            </div>

            <div class="field">
                <label class="label">RAM</label>
                <div class="control">
                    <input type="text" name="recram" id="" class="input" placeholder="Recommended RAM Requirement" required>
                </div>
            </div>

            <div class="field">
                <label class="label">Storage</label>
                <div class="control">
                    <input type="text" name="recstorage" id="" class="input" placeholder="Recommended Storage Requirement" required>
                </div>
            </div>

            <div class="field">
                <label class="label">Operating System</label>
                <div class="control">
                    <input type="text" name="recos" id="" class="input" placeholder="Recommended OS Requirement" required>
                </div>
            </div>

            <div class="field-body">
                <div class="field mt-5">
                    <p class="control">
                        <button class="button is-rounded has-text-white-ter has-background is-fullwidth">Add Game</button>
                    </p>
                </div>
            </div>
        </form>
    </div>
</main>

<script>

    $(document).ready(function() {
        $("#genre").select2();

        $("#genre").change(function(){
            var temp = $("#genre").val();
            $("#genres").val(temp);
        })
    });

</script>
