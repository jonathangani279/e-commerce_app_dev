<form action="{{ url('/api/users/'.Auth::user()->id) }}" enctype="multipart/form-data" method="POST">
    @method('PUT')
    @csrf
    <div class="tag has-background p-5">
        <h5 class="title is-5 has-text-white-ter">Account Information</h5>
    </div>
    <div class="field is-horizontal mt-5">
        <div class="field-body">
            <div class="field">
                <figure class="image is-128x128 container">
                    <img class="is-rounded selected-image" src="{{ Auth::user()->image_path }}" alt="">
                </figure>
                <div class="file is-centered mt-4">
                    <label class="file-label">
                        <input class="file-input" type="file" name="image">
                        <span class="file-cta">
                            <span class="file-icon">
                                <i class="fas fa-upload"></i>
                            </span>
                            <span class="file-label">Change image</span>
                        </span>
                    </label>
                </div>
            </div>
            <div class="field">
                <label class="label">Name</label>
                <p class="control">
                    <input class="input" type="text" name="name_user" value="{{ old('name_user') != '' ? old('name_user') : Auth::user()->name_user }}" placeholder="Name">
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal mt-5">
        <div class="field-body">
            <div class="field">
                <label class="label">Username</label>
                <p class="control has-icons-left">
                    <input class="input" type="text" name="username_user" value="{{ Auth::user()->username_user }}" placeholder="Username" disabled>
                    <span class="icon is-small is-left">
                        <i class="fa fa-user"></i>
                    </span>
                </p>
            </div>
            <div class="field">
                <label class="label">Email</label>
                <p class="control has-icons-left">
                    <input class="input" type="text" name="email_user" value="{{ old('email_user') != '' ? old('email_user') : Auth::user()->email_user }}" placeholder="Email">
                    <span class="icon is-small is-left">
                        <i class="fas fa-envelope"></i>
                    </span>
                </p>
            </div>
        </div>
    </div>
    <div class="is-divider"></div>
    <div class="tag has-background p-5">
        <h5 class="title is-5 has-text-white-ter">Password Information</h5>
    </div>
    <div class="field is-horizontal mt-5">
        <div class="field-body">
            <div class="field">
                <label class="label">Password</label>
                <p class="control has-icons-left">
                    <input class="input" type="password" name="password" value="{{ old('password') != '' ? old('password') : '**********' }}" placeholder="Password">
                    <span class="icon is-small is-left">
                        <i class="fa fa-unlock"></i>
                    </span>
                </p>
            </div>
            <div class="field">
                <label class="label">Confirm - Password</label>
                <p class="control has-icons-left">
                    <input class="input" type="password" name="password_confirmation" placeholder="Confirm - Password">
                    <span class="icon is-small is-left">
                        <i class="fa fa-lock"></i>
                    </span>
                </p>
            </div>
        </div>
    </div>
    <div class="field is-horizontal mt-5">
        <div class="field-body">
            <div class="field">
                <p class="control">
                    <button class="button is-rounded has-text-white-ter has-background is-fullwidth">Save</button>
                </p>
            </div>
            <div class="field">
                <p class="control">
                    <a href="{{ url('/') }}" class="button is-rounded has-text-white-ter has-background is-fullwidth">Cancel</a>
                </p>
            </div>
        </div>
    </div>
</form>

