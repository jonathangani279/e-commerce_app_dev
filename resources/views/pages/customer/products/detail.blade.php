@extends('layouts.base')

@section('extra-css')
    <style></style>
@endsection

@section('title')
    <title>{{ $game->title_game }}</title>
@endsection

@section('content')
    <main>
        <section>
            <figure class="image">
                <img src="{{ asset('/medias/images/heroes/afterparty.jpg') }}">
                <div class="float-center">
                    <div class="has-text-centered">
                        <h1 class="title is-1 has-text-white-ter mb-0">- {{ $game->title_game }} -</h1>
                    </div>
                </div>
            </figure>
            <div class="mt-3">
                <div class="container">
                    <div class="is-flex is-justify-content-center">
                        <a href="#" class="button has-text-white-ter has-background mr-2">
                            BUY NOW &nbsp; <i class="fa fa-gamepad"></i>
                        </a>
                        <a href="#" class="button has-text-white-ter has-background ml-2">
                            ADD TO CART &nbsp; <i class="fa fa-shopping-cart"></i>
                        </a>
                    </div>
                </div>
            </div>
        </section>

        <section>
            <div class="mb-5 py-5">
                <div class="container">
                    <div class="columns">
                        <div class="column">
                            <h5 class="title is-5 has-text-white-ter has-text-centered has-background rounded p-2">Game Details</h5>
                            <div>
                                <figure class="image is-128x128 container">
                                    <img src="{{ $game->publisher->image_path }}" class="is-rounded">
                                </figure>
                                <div class="has-text-centered mt-2">
                                    <b>Publisher: </b>
                                    <span>{{ $game->publisher->name_user }}</span>
                                </div>
                            </div>
                            <div class="separator"></div>
                            <div>
                                <b>Release date: </b>
                                <span>{{ $game->date_game }}</span>
                            </div>
                            <div>
                                <b>Genre: </b>
                                <span class="tag has-text-white has-background">{{ $game->genre_game }}</span>
                            </div>
                            <div>
                                <b>Description: </b>
                                <p class="has-text-justified">
                                    is simply dummy text of the printing and typesetting industry.
                                    Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                                    It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged
                                    It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                                </p>
                            </div>
                        </div>
                        <div class="is-divider-vertical"></div>
                        <div class="column">
                            <h5 class="title is-5 has-text-white-ter has-text-centered has-background rounded p-2">Game Requirements</h5>
                            @if (!is_null($game->minimum_requirement) && !is_null($game->recommended_requirement))
                                <div>
                                    <h5 class="title is-5 mb-2">Minimum Requirement</h5>
                                    <div class="separator"></div>
                                    <div>
                                        <b>Operation system: </b>
                                        <span>{{ $game->minimum_requirement->os_requirement }}</span>
                                    </div>
                                    <div>
                                        <b>Processor: </b>
                                        <span>{{ $game->minimum_requirement->os_requirement }}</span>
                                    </div>
                                    <div>
                                        <b>Graphics: </b>
                                        <span>{{ $game->minimum_requirement->gpu_requirement }}</span>
                                    </div>
                                    <div>
                                        <b>RAM: </b>
                                        <span>{{ $game->minimum_requirement->ram_requirement }}</span>
                                    </div>
                                    <div>
                                        <b>Disk space: </b>
                                        <span>{{ $game->minimum_requirement->storage_requirement }}</span>
                                    </div>
                                </div>
                                <div class="mt-5">
                                    <h5 class="title is-5 mb-2">Recommended Requirement</h5>
                                    <div class="separator"></div>
                                    <div>
                                        <b>Operation system: </b>
                                        <span>{{ $game->recommended_requirement->os_requirement }}</span>
                                    </div>
                                    <div>
                                        <b>Processor: </b>
                                        <span>{{ $game->recommended_requirement->os_requirement }}</span>
                                    </div>
                                    <div>
                                        <b>Graphics: </b>
                                        <span>{{ $game->recommended_requirement->gpu_requirement }}</span>
                                    </div>
                                    <div>
                                        <b>RAM: </b>
                                        <span>{{ $game->recommended_requirement->ram_requirement }}</span>
                                    </div>
                                    <div>
                                        <b>Disk space: </b>
                                        <span>{{ $game->recommended_requirement->storage_requirement }}</span>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection

@section('extra-js')
    <script></script>
@endsection
