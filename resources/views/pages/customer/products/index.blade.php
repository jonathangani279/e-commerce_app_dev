@extends('layouts.base')

@section('title')
    <title>Products</title>
@endsection

@section('extra-css')
    <style></style>
@endsection

@section('content')
    <main>
        <section>
            <figure class="image is-3by1">
                <img src="{{ asset('/medias/images/heroes/overwatch.jpg') }}">
                <div class="float-center">
                    <div class="has-text-centered">
                        <h1 class="title is-1 has-text-white-ter mb-0">- GAMES -</h1>
                        <span class="has-text-white-ter">Find your happiness</span>
                    </div>
                </div>
            </figure>
        </section>

        <section>
            <div class="mt-5 mb-5 pt-5 pb-5">
                <div class="container">
                    <form action="" method="GET">
                        <div class="field is-horizontal">
                            <div class="field-body">
                                <div class="field mr-0">
                                    <label class="label">Game Title</label>
                                    <p class="control">
                                        <input class="input" type="text" placeholder="Star Wars, Doom, etc..." autofocus>
                                    </p>
                                </div>
                                <div class="field mr-0">
                                    <label class="label">Publisher Name</label>
                                    <p class="control">
                                        <input class="input" type="text" placeholder="Sony, Bandai, etc...">
                                    </p>
                                </div>
                                <div class="field mr-0">
                                    <label class="label">Release Date</label>
                                    <p class="control">
                                        <input class="input" type="date">
                                    </p>
                                </div>
                                <div class="field is-narrow mr-0">
                                    <label class="label">
                                        <span>Genre</span>
                                    </label>
                                    <p class="control">
                                        <div class="select">
                                            <select name="">
                                                <option value="" selected>Adventure</option>
                                                <option value="">Fighting</option>
                                                <option value="">FPS Shooting</option>
                                            </select>
                                        </div>
                                    </p>
                                </div>
                                <div class="field mr-0">
                                    <label class="label has-text-white-ter">''</label>
                                    <p class="control">
                                        <button class="button is-fullwidth has-text-white-ter has-background" type="submit">
                                            Search
                                        </button>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="container" id="container-spinner">
                    <div id="products-list"></div>
                </div>
            </div>
        </section>
    </main>
@endsection

@section('extra-js')
    <script>
        let spinner= new jQuerySpinner({
            parentId: 'container-spinner',
            duration: 500
        });

        spinner.show();

        let fetch_games= $.ajax({
            url: '{{ url("/api/games/list") }}',
            type: 'GET',
            dataType: 'json'
        });
        let fetch_list_view= fetch_games.done(function(games) {
            $.ajax({
                url: '{{ url("/sub-view/products/list") }}',
                type: 'GET',
                data: { games: games },
                success: function(list_view) {
                    spinner.hide();
                    $('#products-list').html(list_view);
                }
            });
        });

        $(document).ajaxStop(function() {
            $('#container-load-more').simpleLoadMore({
                item: '.column',
                count: 6,
                btnHTML: `
                    <div class="container mt-5">
                        <div class="has-text-centered">
                            <button class="button button-load-more is-rounded has-background">
                                <h5 class="title is-5 has-text-white-ter button-load-more-text">Show More</h5>
                            </button>
                        </div>
                    </div>
                `
            });

            $('.button-load-more').click(function() {
                spinner.show();
                spinner.hide();
            });
        });
    </script>
@endsection
