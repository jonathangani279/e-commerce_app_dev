<div class="columns is-multiline mt-5" id="container-load-more">
    @foreach ($games as $game)
        @include('components.game', [
            'game' => $game,
            'css_class' => 'is-4'
        ])
    @endforeach
</div>
