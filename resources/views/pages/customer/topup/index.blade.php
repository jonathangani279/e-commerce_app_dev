@extends('layouts.base')

@section('extra-css')
    <style></style>
@endsection

@section('title')
    <title>Administration</title>
@endsection

@section('content')
    <main class="py-6 mx-3">
        <input type="number" id="amount" id="">
        <button id="topup" onclick="topUpClick()">Top UP</button>
    </main>
@endsection

@section('extra-js')
<script>
    function topUpClick(){
        $.ajax({
            type: "POST",
            url: "{{ url('api/payment/getSnapToken') }}",
            data: {
                amount : $("#amount").val(),
                type : 'Top up wallet',
            },
            success: function (response) {
                snap.pay(response);
            }
        });
    }
</script>
@endsection
