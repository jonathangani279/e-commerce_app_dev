@extends('layouts.base');

@section('extra-css')
    <style></style>
@endsection

@section('title')
    <title>Cart</title>
@endsection

@section('content')
    <main>
        <section>
            <div class="mt-5 pt-5 mb-5 pb-5">
                <div class="container">
                    <h1 class="title is-1 has-text-black-ter">Cart</h1>
                    <div class="columns is-multiline">
                        <figure class="imghvr-flip-diag-1 box">
                            <img src="https://cdn.mos.cms.futurecdn.net/6YDMcHZCvPZJ5HhZfjRQSN-320-80.jpg" class="game-box">
                            <figcaption class="game-hover">
                                <div class="container has-text-centered">
                                    <h5 class="title is-5 mb-5">
                                        Game 2 - 2000
                                        {{-- {{ $game->title_game }} - {{ $game->price_game }} --}}
                                    </h5>
                                    <a href="#" class="button is-rounded has-text-white mb-2">
                                        Go Check Details! <i class="fas fa-running ml-1"></i>
                                    </a>
                                    <a href="#" class="button is-rounded has-text-white mt-2 mb-2">
                                        Delete From Cart <i class="fa fa-shopping-cart ml-1"></i>
                                    </a>
                                    <a href="#" class="button is-rounded has-text-white mt-2">
                                        Wishing for This! <i class="fa fa-star ml-1"></i>
                                    </a>
                                </div>
                            </figcaption>
                        </figure>
                    </div>
                    <button class="button is-rounded has-text-white has-background mt-2 mb-2 " style="">Checkout</button>
                </div>
            </div>
        </section>
    </main>
@endsection


@section('extra-js')
    <script></script>
@endsection

