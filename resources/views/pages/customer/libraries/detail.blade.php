<figure class="image 2by1">
    <img src="{{ asset('/medias/images/heroes/cyberpunk-2077-3.jpg') }}">
</figure>
<button class="button has-text-white-ter has-background mt-3">
    Download &nbsp; <i class="fa fa-download"></i>
</button>
<div class="level mt-2 mb-0">
    <div class="level-left">
        <div class="level-item">
            <h3 class="title is-3">{{ $library->game->title_game }}</h3>
        </div>
    </div>
    <div class="level-right">
        <div class="level-item">
            <b>Publisher: </b> &nbsp;
            <span>{{ $library->game->publisher->name_user }}</span>
        </div>
    </div>
</div>
<div class="mt-1">
    <b>Release date: </b>
    <span>{{ $library->game->date_game }}</span>
</div>
<div class="mt-1">
    <b>Genre: </b>
    <span class="tag has-text-white-ter has-background">{{ $library->game->genre_game }}</span>
</div>
<p class="has-text-justified mt-1">
    is simply dummy text of the printing and typesetting industry.
    Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
    It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged
    It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
</p>
@if (!is_null($library->game->minimum_requirement) && !is_null($library->game->recommended_requirement))
    <div class="columns mt-1">
        <div class="column">
            <h5 class="title is-5 mb-0">Minimum Requirement</h5>
            <div class="mt-2">
                <b>Operation system: </b>
                <span>{{ $library->game->minimum_requirement->os_requirement }}</span>
            </div>
            <div class="mt-1">
                <b>Processor: </b>
                <span>{{ $library->game->minimum_requirement->cpu_requirement }}</span>
            </div>
            <div class="mt-1">
                <b>Graphics: </b>
                <span>{{ $library->game->minimum_requirement->gpu_requirement }}</span>
            </div>
            <div class="mt-1">
                <b>RAM: </b>
                <span>{{ $library->game->minimum_requirement->ram_requirement }}</span>
            </div>
            <div class="mt-1">
                <b>Disk space: </b>
                <span>{{ $library->game->minimum_requirement->storage_requirement }}</span>
            </div>
        </div>
        <div class="is-divider-vertical p-0"></div>
        <div class="column">
            <h5 class="title is-5 mb-0">Recommended Requirement</h5>
            <div class="mt-2">
                <b>Operation system: </b>
                <span>{{ $library->game->recommended_requirement->os_requirement }}</span>
            </div>
            <div class="mt-1">
                <b>Processor: </b>
                <span>{{ $library->game->recommended_requirement->cpu_requirement }}</span>
            </div>
            <div class="mt-1">
                <b>Graphics: </b>
                <span>{{ $library->game->recommended_requirement->gpu_requirement }}</span>
            </div>
            <div class="mt-1">
                <b>RAM: </b>
                <span>{{ $library->game->recommended_requirement->ram_requirement }}</span>
            </div>
            <div class="mt-1">
                <b>Disk space: </b>
                <span>{{ $library->game->recommended_requirement->storage_requirement }}</span>
            </div>
        </div>
    </div>
@endif
