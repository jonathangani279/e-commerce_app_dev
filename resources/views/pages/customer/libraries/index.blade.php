@extends('layouts.base')

@section('extra-css')
    <style>
        #libraries-dashboard {
            height: fit-content;
        }

        #libraries-dashboard h6 {
            border-bottom: 1px solid var(--theme-color);
        }

        #libraries-dashboard h6:hover {
            background-color: var(--theme-color);
            color: white;
            border-radius: 5px;
            cursor: pointer;
        }
    </style>
@endsection

@section('title')
    <title>My Libraries</title>
@endsection

@section('content')
    <main>
        <div class="mt-5 mb-5 pt-5 pb-5">
            <div class="container">
                <div class="columns">
                    <div class="column is-3 box container mr-4 p-4" id="libraries-dashboard">
                        <div class="field">
                            <p class="control has-icons-right">
                                <input class="input" type="text" placeholder="Search game" autofocus>
                                <span class="icon is-small is-right">
                                    <i class="fas fa-search"></i>
                                </span>
                            </p>
                        </div>
                        <div class="tag has-background mt-1 p-4">
                            <h6 class="title is-6 has-text-white-ter">My Games</h6>
                        </div>
                        <div id="libraries-list"></div>
                    </div>
                    <div class="column is-three-fifths container box ml-4 p-4" id="container-spinner">
                        <div id="libraries-detail"></div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection

@section('extra-js')
    <script>
        function load_detail(id_library= 1) {
            spinner.show();

            let fetch_library= $.ajax({
                url: `/api/libraries/${id_library}`,
                type: 'GET'
            });
            let fetch_detail_view= fetch_library.done(function(library) {
                $.ajax({
                    url: '/sub-view/libraries/detail',
                    type: 'GET',
                    data: { library: library },
                    success: function(sub_view) {
                        spinner.hide();
                        $('#libraries-detail').html(sub_view);
                        $('#libraries-dashboard h6').unbind();
                    }
                });
            });
        }

        let spinner= new jQuerySpinner({
            parentId: 'container-spinner',
            duration: 500
        });
        let fetch_libraries= $.ajax({
            url: '{{ url("/api/libraries/list") }}',
            type: 'GET',
            dataType: 'json'
        });
        let fetch_list_view= fetch_libraries.done(function(libraries) {
            $.ajax({
                url: '/sub-view/libraries/list',
                type: 'GET',
                data: { libraries: libraries },
                success: function(sub_view) {
                    $('#libraries-list').html(sub_view);
                }
            });
        });

        load_detail();

        $(document).ajaxStop(function() {
            $('#libraries-dashboard h6').click(function() {
                $('#libraries-dashboard h6').each(function() {
                    $(this).removeClass('aside-item-clicked');
                });
                spinner.show();
                load_detail($(this).attr('id_library'));
                $(this).addClass('aside-item-clicked');
            });
        });
    </script>
@endsection


