<div>
    <table class="table is-fullwidth is-hoverable">
        <thead>
            <th>No</th>
            <th>Image</th>
            <th>Title</th>
            <th>Price</th>
            <th>Publisher</th>
            <th>#Sold</th>
            <th>Date</th>
            <th></th>
        </thead>
        <tbody id='table-game'></tbody>
    </table>
</div>

<script>
    $(document).ready(function () {
        refreshMasterGame();
    });

    function refreshMasterGame(){
        $("#table-game").html("");
        $.get("{{url('/api/admin/mastergame')}}",
            function (data) {
                data.listGame.forEach((element,index) => {
                    let icon_status = "";
                    if(element.status_game==1){
                        icon_status = "fa-toggle-on";
                    }else{
                        icon_status = "fa-toggle-off";
                    }
                    $("#table-game").append(
                        `<tr>
                            <td>${index+1}</td>
                            <td></td>
                            <td>${element.title_game}</td>
                            <td>${element.price_game}</td>
                            <td>${element.publisher.name_user}</td>
                            <td></td>
                            <td>${element.created_at}</td>
                            <td><span onclick="toggleStatusGame(${element.id_game},${Math.abs(element.status_game-1)})"><i class="fas ${icon_status}"></i></span></td>
                        </tr>`
                    )
                });
            }
        );
    }

    function toggleStatusGame(id_game,state){
        $.ajax({
            type: "post",
            url: "{{ url('api/admin/toggleStatusGame') }}",
            data: {
                id_game : id_game,
                state : state
            },
            success: function (response) {
                console.log(response);
                refreshMasterGame();
            }
        });
    }
</script>
