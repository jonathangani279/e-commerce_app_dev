<div class="columns is-mobile is-multiline is-centered">
    <div class="column box mr-3 mb-3 is-5">
        <div class="is-vcentered is-half has-text-centered content" style="height:200px">
            <p class="heading">Total User</p>
            <p class="title" id='total-user-number'></p>
        </div>
    </div>
    <div class="column box mr-3 mb-3 is-5">
        <div class="is-vcentered is-half has-text-centered content" style="height:200px">
            <p class="heading">Total Game</p>
            <p class="title" id='total-game-number'></p>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $.get("{{ url('/api/admin/dashboard') }}",
            function (data) {
                $("#total-user-number").text(data["total_user"]);
                $("#total-game-number").text(data["total_game"]);
            }
        );
    });

</script>
