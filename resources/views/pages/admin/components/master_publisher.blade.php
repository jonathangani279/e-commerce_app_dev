<div>
    <table class="table is-fullwidth is-hoverable">
        <thead>
            <tr>
                <th>No</th>
                <th>Nama</th>
                <th></th>
            </tr>
        </thead>
        <tbody id="table-master-publisher">

        </tbody>
    </table>
</div>

<script>
    $(document).ready(function () {
        refreshMasterPublisher();
    });
    function refreshMasterPublisher (){
        $.get("{{ url('api/admin/getPublishers') }}",
            function (data) {
                $("#table-master-publisher").html("");
                data.forEach((element,index) => {
                    let iconStatus = "";
                    if(element.status_user==1){
                        iconStatus ="fa-toggle-on";
                    }else{
                        iconStatus = "fa-toggle-off";
                    }
                    $("#table-master-publisher").append(
                        `
                            <tr>
                                <td>${index+1}</td>
                                <td>${element.name_user}</td>
                                <td><span onclick="toggleStatusPublisher(${element.id},${Math.abs(element.status_user-1)})"><i class="fas ${iconStatus}"></i></span></td>
                            </tr>
                        `
                    )
                });
            }
        );
    }

    function toggleStatusPublisher(id_user,status){
        $.ajax({
            type: "PUT",
            url: "{{ url('api/admin/toggleStatusPublisher') }}",
            data: {
                id_user : id_user,
                status : status
            },
            success: function (response) {
                refreshMasterPublisher();
                refreshMasterGame();
            }
        });
    }
</script>
