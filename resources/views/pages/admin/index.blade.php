@extends('layouts.base')

@section('extra-css')
    <style></style>
@endsection

@section('title')
    <title>Administration</title>
@endsection

@section('content')
    <main class="py-6 mx-3">
        <div class="max-width">
            <div class="columns is-multiline is-mobile">
                <div class="column is-one-quarter">
                    <aside class="menu">
                        <ul class="menu-list" onclick="openDashboard()">
                            <li>
                                <a id="label-dashboard">Dashboard</a>
                            </li>
                        </ul>
                        <p class="menu-label">
                            Administration
                        </p>
                        <ul class="menu-list">
                            <li onclick="openMasterPublisher()">
                                <a id="label-master-publisher">Master Publisher</a>
                            </li>
                            <li onclick="openMasterGame()">
                                <a id="label-master-game">Master Game</a>
                            </li>
                            <li onclick="openMasterTransaction()">
                                <a id="label-master-transaction">Master Transaction</a>
                            </li>
                        </ul>
                    </aside>
                </div>
                <div class="column">
                    <div class="is-hidden" id="dashboard-box">
                        @include('pages.admin.components.dashboard')
                    </div>
                    <div class="is-hidden" id="master-publisher-box">
                        @include('pages.admin.components.master_publisher')
                    </div>
                    <div class="is-hidden" id="master-game-box">
                        @include('pages.admin.components.master_game')
                    </div>
                    <div class="is-hidden" id="master-transaction-box">
                        @include('pages.admin.components.master_transaction')
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection

@section('extra-js')
    <script>
        let currentOpenBox = null;
        let currentOpenLabel = null;

        function openDashboard(){
            if(currentOpenBox){
                currentOpenBox.addClass("is-hidden");
                currentOpenLabel.removeClass("is-active");
            }
            $("#dashboard-box").removeClass("is-hidden");
            $("#label-dashboard").addClass("is-active");
            currentOpenBox = $("#dashboard-box");
            currentOpenLabel = $("#label-dashboard")
        }

        function openMasterPublisher(){
            if(currentOpenBox){
                currentOpenBox.addClass("is-hidden");
                currentOpenLabel.removeClass("is-active");
            }
            $("#master-publisher-box").removeClass("is-hidden");
            $("#label-master-publisher").addClass("is-active");
            currentOpenBox = $("#master-publisher-box");
            currentOpenLabel = $("#label-master-publisher");
        }

        function openMasterGame(){
            if(currentOpenBox){
                currentOpenBox.addClass("is-hidden");
                currentOpenLabel.removeClass("is-active");
            }
            $("#master-game-box").removeClass("is-hidden");
            $("#label-master-game").addClass("is-active");
            currentOpenBox = $("#master-game-box");
            currentOpenLabel = $("#label-master-game");
        }

        function openMasterTransaction(){
            if(currentOpenBox){
                currentOpenBox.addClass("is-hidden");
                currentOpenLabel.removeClass("is-active");
            }
            $("#master-transaction-box").removeClass("is-hidden");
            $("#label-master-transaction").addClass("is-active");
            currentOpenBox = $("#master-transaction-box");
            currentOpenLabel = $("#label-master-transaction");
        }
    </script>
@endsection
