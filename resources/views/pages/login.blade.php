@extends('layouts.base')

@section('extra-css')
    <style>
        #form-login {
            width: 35%;
        }
    </style>
@endsection

@section('title')
    <title>Login</title>
@endsection

@section('content')
    <main>
        <section>
            <figure class="image is-3by1">
                <img src="{{ asset('/medias/images/heroes/cyberpunk-2077-1.jpg') }}">
                <div class="float-center">
                    <div class="has-text-centered">
                        <h1 class="title is-1 has-text-white-ter mb-0">- SIGN IN -</h1>
                        <span class="has-text-white-ter">Get excited</span>
                    </div>
                </div>
            </figure>
        </section>

        <section>
            <div class="mt-5 pt-5 mb-5 pb-5">
                <div class="box container p-5" id="form-login">
                    <form action="{{ url('/api/login') }}" method="POST">
                        @csrf
                        <div class="field">
                            <label class="label">Username</label>
                            <p class="control has-icons-left">
                                <input class="input" type="text" name="username" value="{{ old('username') }}" placeholder="Username" autofocus>
                                <span class="icon is-small is-left">
                                    <i class="fa fa-user"></i>
                                </span>
                            </p>
                        </div>
                        <div class="field">
                            <label class="label">Password</label>
                            <p class="control has-icons-left">
                                <input class="input" type="password" name="password" value="{{ old('password') }}" placeholder="Password">
                                <span class="icon is-small is-left">
                                    <i class="fa fa-lock"></i>
                                </span>
                            </p>
                        </div>
                        <div class="field">
                            <p class="control">
                                <button class="button is-rounded has-text-white-ter has-background is-fullwidth" type="submit">
                                    Login
                                </button>
                            </p>
                        </div>
                        <div class="field">
                            <p class="control">
                                <a href="{{ url('/auth/google') }}" class="button is-info is-rounded is-fullwidth">
                                    <span class="icon">
                                        <i class="fab fa-google"></i>
                                    </span>
                                    <span>Sign in with Google</span>
                                </a>
                            </p>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </main>
@endsection

@section('extra-js')
    <script></script>
@endsection
