@extends('layouts.base')

@section('extra-css')
    <style>
        #form-register {
            width: 55%;
        }
    </style>
@endsection

@section('title')
    <title>Register</title>
@endsection

@section('content')
    <main>
        <section>
            <figure class="image is-3by1">
                <img src="{{ asset('/medias/images/heroes/cyberpunk-2077-2.png') }}">
                <div class="float-center">
                    <div class="has-text-centered">
                        <h1 class="title is-1 has-text-white-ter mb-0">- CREATE AN ACCOUNT -</h1>
                        <span class="has-text-white-ter">One step closer</span>
                    </div>
                </div>
            </figure>
        </section>

        <section>
            <div class="mt-5 pt-5 mb-5 pb-5">
                <div class="box container p-5" id="form-register">
                    <form action="{{ url("/api/register") }}" method="POST">
                        @csrf
                        <div class="field">
                            <label class="label">Name</label>
                            <p class="control">
                                <input class="input" type="text" name="name_user" value="{{ old('name_user') }}" placeholder="Name" autofocus>
                            </p>
                        </div>
                        <div class="field is-horizontal">
                            <div class="field-body">
                                <div class="field">
                                    <label class="label">Username</label>
                                    <p class="control has-icons-left">
                                        <input class="input" type="text" name="username_user" value="{{ old('username_user') }}" placeholder="Username">
                                        <span class="icon is-small is-left">
                                            <i class="fa fa-user"></i>
                                        </span>
                                    </p>
                                </div>
                                <div class="field">
                                    <label class="label">Email</label>
                                    <p class="control has-icons-left">
                                        <input class="input" type="text" name="email_user" value="{{ old('email_user') }}" placeholder="Email">
                                        <span class="icon is-small is-left">
                                            <i class="fas fa-envelope"></i>
                                        </span>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="field is-horizontal">
                            <div class="field-body">
                                <div class="field">
                                    <label class="label">Password</label>
                                    <p class="control has-icons-left">
                                        <input class="input" type="password" name="password" value="{{ old('password') }}" placeholder="Password">
                                        <span class="icon is-small is-left">
                                            <i class="fa fa-unlock"></i>
                                        </span>
                                    </p>
                                </div>
                                <div class="field">
                                    <label class="label">Confirm - Password</label>
                                    <p class="control has-icons-left">
                                        <input class="input" type="password" name="password_confirmation" value="{{ old('password_confirmation') }}" placeholder="Confirm - Password">
                                        <span class="icon is-small is-left">
                                            <i class="fa fa-lock"></i>
                                        </span>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="field">
                            <label class="label">Join as</label>
                            <p class="control">
                                <div class="select">
                                    <select name="role_user">
                                        <option value="1" selected>Customer</option>
                                        <option value="2">Publisher</option>
                                    </select>
                                </div>
                            </p>
                        </div>
                        <div class="field is-horizontal">
                            <div class="field-body">
                                <div class="field">
                                    <p class="control">
                                        <button type="submit" class="button is-rounded is-fullwidth has-text-white-ter has-background">Register</button>
                                    </p>
                                </div>
                                <div class="field">
                                    <p class="control">
                                        <a href="{{ url('/auth/google') }}" class="button is-info is-rounded is-fullwidth">
                                            <span class="icon">
                                                <i class="fab fa-google"></i>
                                            </span>
                                            <span>Sign in with Google</span>
                                        </a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </main>
@endsection

@section('extra-js')
    <script></script>
@endsection
