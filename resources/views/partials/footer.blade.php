<footer>
    <div class="has-background-black-bis">
        <div class="container has-text-grey-light">
            <div class="columns pt-6 pb-6">
                <div class="column is-3">
                    <img src="https://via.placeholder.com/150" alt="">
                </div>
                <div class="column is-3">
                    <h4 class="title is-4 has-text-white">GAME BUCKET</h4>
                    <p class="mt-5 mb-2">
                        We create platform <br> for connected world
                    </p>
                    <span class="has-text-white">Be Pros.</span>
                </div>
                <div class="column is-3 ml-6">
                    <h5 class="title is-5 has-text-white">Resources.</h5>
                    <ul>
                        <li class="pb-1">
                            <a href="" class="has-text-grey-light">About Us</a>
                        </li>
                        <li class="pt-1 pb-1">
                            <a href="" class="has-text-grey-light">FAQ</a>
                        </li>
                        <li class="pt-1 pb-1">
                            <a href="" class="has-text-grey-light">How to Buy</a>
                        </li>
                        <li class="pt-1">
                            <a href="" class="has-text-grey-light">Terms & Conditions</a>
                        </li>
                    </ul>
                </div>
                <div class="column is-3">
                    <h5 class="title is-5 has-text-white">Connect With Us.</h5>
                    <ul>
                        <li class="pb-1">
                            <a href="" class="has-text-white">
                                <i class="fab fa-facebook-f"></i>
                                <span class="has-text-grey-light">Facebook</span>
                            </a>
                        </li>
                        <!--<li class="pt-1 pb-1">
                            <a href="" class="has-text-white">
                                <i class="fab fa-twitter"></i>
                                <span class="has-text-grey-light">Twitter</span>
                            </a>
                        </li>-->
                        <li class="pt-1 pb-1">
                            <a href="" class="has-text-white">
                                <i class="fab fa-instagram"></i>
                                <span class="has-text-grey-light">Instagram</span>
                            </a>
                        </li>
                        <li class="pt-1">
                            <a href="" class="has-text-white">
                                <i class="fab fa-youtube"></i>
                                <span class="has-text-grey-light">YouTube</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="has-background-black-ter has-text-centered p-4">
        <span class="has-text-white is-size-7">
            &copy;Copyright 2020 Game Bucket. All Rights Reserved.
        </span>
    </div>
</footer>
