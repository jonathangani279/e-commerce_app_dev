<header>
    <section>
        <div class="has-background-black-bis is-fixed-top">
            <div class="level p-3">
                <div class="level-left"></div>
                <div class="level-right">
                    <div class="level-item">
                        <a class="has-text-white is-size-6 pr-4" data-izimodal-open="#modal" data-izimodal-transitionin="fadeInDown">
                            <i class="fas fa-user"></i>
                        </a>
                        <a href="{{ url('/libraries') }}" class="has-text-white is-size-6 px-4">
                            <i class="fas fa-gamepad"></i>
                        </a>
                        <a href="{{ url('/cart') }}" class="has-text-white is-size-6 px-4">
                            <i class="fas fa-shopping-cart"></i>
                        </a>

                        <a href="" class="has-text-white is-size-6 px-4">
                            <i class="fas fa-star"></i>
                        </a>
                        <a href="" class="has-text-white is-size-6 pl-4">
                            <i class="fas fa-question"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div id="modal">
            <div class="p-5 has-background-black-bis">
                <div class="columns has-text-centered">
                    <div class="column">
                        @guest
                            <a href="{{ url('/login') }}">
                                <figure class="image container is-128x128">
                                    <img class="is-rounded" src="{{ asset('/medias/images/icons/sign-in.svg') }}" alt="">
                                </figure>
                                <h5 class="title is-5 has-text-white mt-3">Sign In</h5>
                            </a>
                        @endguest

                        @auth
                            <a href="{{ url('/profile') }}">
                                <figure class="image is-128x128 container">
                                    <img class="is-rounded" src="{{ Auth::user()->image_path }}" alt="">
                                </figure>
                                <h5 class="title is-5 has-text-white mt-3">My Profile</h5>
                            </a>
                        @endauth
                    </div>
                    <div class="is-divider-vertical has-text-centered"></div>
                    <div class="column">
                        @guest
                            <a href="{{ url('/register') }}">
                                <figure class="image container is-128x128">
                                    <img class="is-rounded" src="{{ asset('/medias/images/icons/register.svg') }}" alt="">
                                </figure>
                                <h5 class="title is-5 has-text-white mt-3">Create an Account</h5>
                            </a>
                        @endguest

                        @auth
                            <a href="{{ url('/logout') }}">
                                <i class="fas fa-sign-out-alt fa-8x text-color"></i>
                            </a>
                        @endauth
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section>
        @include('partials.navbar')
    </section>
</header>
