<div class="box">
    <nav class="navbar" role="navigation">
        <div class="navbar-menu">
            <div class="navbar-start">
                <div class="navbar-brand">
                    <a href="{{ url('/') }}">
                        <img src="https://via.placeholder.com/75" alt="">
                    </a>
                </div>
                <a href="{{ url('/') }}" class="navbar-item">Home</a>
                <div class="navbar-item has-dropdown is-hoverable">
                    <a href="" class="navbar-link">Games</a>
                    <div class="navbar-dropdown">
                        <a href="" class="navbar-item">Featured Games</a>
                        <a href="" class="navbar-item">All Games</a>
                        <a href="" class="navbar-item">Free-To-Play</a>
                    </div>
                </div>
                <div class="navbar-item has-dropdown is-hoverable">
                    <a href="" class="navbar-link">Genres</a>
                    <div class="navbar-dropdown">
                        <a href="" class="navbar-item">Adventure</a>
                        <a href="" class="navbar-item">Fighting</a>
                        <a href="" class="navbar-item">FPS Shooting</a>
                    </div>
                </div>
                <a href="" class="navbar-item">About</a>
            </div>
        </div>
    </nav>
</div>
