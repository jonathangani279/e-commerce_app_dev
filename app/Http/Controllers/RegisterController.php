<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

use App\Models\UserModel;

class RegisterController extends Controller
{
    private $rules= [
        'name_user' => 'required',
        'username_user' => 'required',
        'email_user' => 'required|email|unique:users',
        'password' => 'required|confirmed',
        'role_user' => 'required'
    ];

    public function index(Request $request) {
        $fields= $request->all();
        $fields['password_user']= $fields['password'];
        $fields['status_user']= 1;

        $validator= Validator::make($fields, $this->rules);
        $error_message= '';

        if ($validator->fails()) {
            foreach ($validator->errors()->messages() as $key => $fields) {
                foreach ($fields as $key => $error) {
                    $error_message.= $error."\n";
                }
            }

            alert()->error($error_message, 'Error Input');

            return back()->withInput();
        }

        if (UserModel::create($fields)) {
            alert()->success('Account created!', 'Successfully Register');

            return back();
        }
    }
}
