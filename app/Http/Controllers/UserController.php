<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

use App\Models\UserModel;

class UserController extends Controller
{
    private $rules= [
        'name_user' => 'required',
        'email_user' => 'required|email',
        'password' => 'required',
    ];

    public function update(Request $request, $id) {
        $fields= $request->all();

        if ($fields['password'] != '**********') {
            $this->rules['password'].= '|confirmed';
            $fields['password_user']= Hash::make($fields['password']);
        } else {
            unset($this->rules['password']);
            unset($fields['password']);
            unset($fields['password_confirmation']);
        }

        if ($request->has('image')) {
            $this->rules['image']= 'image';
            $fields['image_path']= config('app.url').'/medias/images/uploads/'.$fields['image']->getClientOriginalName();
        }

        $validator= Validator::make($fields, $this->rules);
        $error_message= '';

        if ($validator->fails()) {
            foreach ($validator->errors()->messages() as $key => $fields) {
                foreach ($fields as $key => $error) {
                    $error_message.= $error."\n";
                }
            }

            alert()->error($error_message, 'Required Fields');

            return back()->withInput();
        }

        if ($request->image) {
            $fields['image']->move(public_path('/medias/images/uploads'), $fields['image']->getClientOriginalName());
        }

        if (UserModel::find($id)->update($fields)) {
            alert()->success('Successfully saved!', 'Save Profile');

            return back();
        }
    }
}
