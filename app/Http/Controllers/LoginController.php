<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

use App\Models\UserModel;

class LoginController extends Controller
{
    private $rules= [
        'username' => 'required',
        'password' => 'required',
    ];

    public function index (Request $request) {
        $fields= $request->all();

        $validator= Validator::make($fields, $this->rules);
        $error_message= '';

        if ($validator->fails()) {
            foreach ($validator->errors()->messages() as $key => $fields) {
                foreach ($fields as $key => $error) {
                    $error_message.= $error."\n";
                }
            }

            alert()->error($error_message, 'Required Fields');

            return back()->withInput();
        }

        if ($request->username == 'admin' && $request->password == 'admin') {
            alert()->success('Welcome, Admin!', 'Successfully Login');

            return redirect ('/admin');
        }

        $user = UserModel::where('username_user', $request->username)->get()->first();

        if (!$user) {
            alert()->error('Wrong username or password!', 'Error Credentials');

            return back()->withInput();
        }

        if (Hash::check($request->password, $user->password_user)) {
            Auth::login($user, true);
            alert()->success('Welcome, '.Auth::user()->name_user.'!', 'Successfully Login');

            return redirect ('/');
        }
    }
}
