<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\GameModel;

class ViewController extends Controller
{
    public function landing() {
        $games= GameModel::select('id_game', 'title_game', 'price_game', 'image_path', 'url_path')->limit(6)->get();

        return view('pages.landing', ['games' => $games]);
    }

    public function login() {
        return view('pages.login');
    }

    public function register() {
        return view('pages.register');
    }

    public function admin() {
        $data['listGame'] = GameModel::with("publisher")->get();

        return view('pages.admin.index');
    }

    public function publisher() {
        return view('pages.publisher.index');
    }

    public function products() {
        return view('pages.customer.products.index');
    }

    public function products_detail() {
        $game= GameModel::where('url_path', url()->current())->with([
            'publisher',
            'minimum_requirement',
            'recommended_requirement'
        ])->first();

        return view('pages.customer.products.detail', ['game' => $game]);
    }

    public function libraries() {
        return view('pages.customer.libraries.index');
    }

    public function profile() {
        return view('pages.customer.profile.index');
    }

    public function topup(){
        return view('pages.customer.topup.index');
    }
    public function cart(){
        return view('pages.customer.carts.cart');
    }
}
