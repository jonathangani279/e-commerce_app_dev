<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Models\LibraryModel;

class LibraryController extends Controller
{
    public function index(Request $request) {
        $libraries= LibraryModel::where('id_customer', Auth::user()->id)->with('game')->get();

        return response()->json($libraries);
    }

    public function list() {
        $libraries= LibraryModel::select('id_library', 'id_product')->where('id_customer', Auth::user()->id)->with(['game' => function($query) {
            $query->select('id_game', 'title_game');
        }])->get();

        return response()->json($libraries);
    }

    public function show($id) {
        $library= LibraryModel::where('id_library', $id)->with(['game'])->first();

        return response()->json($library);
    }
}
