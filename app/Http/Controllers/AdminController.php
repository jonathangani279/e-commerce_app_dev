<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\UserModel;
use App\Models\GameModel;

class AdminController extends Controller
{
    public function dashboard(Request $request){
        $total_user = UserModel::where('status_user',1)->get()->count();
        $total_game = GameModel::all()->count();
        return ["total_user"=>$total_user,"total_game"=>$total_game];
    }

    public function masterGame(Request $request){
        $data['listGame'] = GameModel::with("publisher")->get();

        return $data;
    }

    public function toggleStatusGame(Request $request){
        $result = GameModel::where("id_game",$request->id_game)->update(["status_game"=>$request->state]);

        return $result;
    }

    public function getPublishers(Request $request){
        return UserModel::where("role_user",2)->with("games")->get();
    }

    public function toggleStatusPublisher(Request $request){
        UserModel::where("id",$request->id_user)->update(["status_user"=>$request->status]);
        GameModel::where('id_publisher',$request->id_user)->update(["status_game"=>$request->status]);
    }
}
