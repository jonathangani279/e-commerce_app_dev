<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Models\GameModel;
use App\Models\RequirementModel;

class GameController extends Controller
{
    public function index(Request $request) {
        $games= GameModel::with([
            'publisher',
            'minimum_requirement',
            'recommended_requirement'
        ])->get();

        return response()->json($games);
    }

    public function list() {
        $games= GameModel::select('id_game', 'title_game', 'price_game', 'image_path', 'url_path')->get();

        return response()->json($games);
    }

    public function show($id) {
        $game= GameModel::where('id_game', $id)->with([
            'publisher',
            'minimum_requirement',
            'recommended_requirement'
        ])->first();

        return response()->json($game);
    }

    public function addGame(Request $request) {
        $fields= $request->all();
        $mincpu= $request->input("mincpu");
        $mingpu= $request->input("mingpu");
        $minram= $request->input("minram");
        $minos= $request->input("minos");
        $minstorage= $request->input("minstorage");

        $tempMinimumReq = [
            'cpu_requirement'=>$mincpu,
            'gpu_requirement'=>$mingpu,
            'ram_requirement'=>$minram,
            'storage_requirement'=>$minstorage,
            'os_requirement'=>$minos
        ];

        $newMinReq= new RequirementModel($tempMinimumReq);
        $newMinReq->save();

        $requirementID= DB::table("requirements")->select("*")->get();
        $idCountMinimum= count($requirementID);

        $reccpu= $request->input("reccpu");
        $recgpu= $request->input("recgpu");
        $recram= $request->input("recram");
        $recos= $request->input("recos");
        $recstorage= $request->input("recstorage");

        $tempRecommendedReq = [
            'os_requirement' => $recos,
            'cpu_requirement' => $reccpu,
            'gpu_requirement' => $recgpu,
            'ram_requirement' => $recram,
            'storage_requirement' => $recstorage,
        ];

        $newrecReq= new RequirementModel($tempRecommendedReq);
        $newrecReq->save();

        $requirementID= DB::table("requirements")->select("*")->get();
        $idCountRecommended= count($requirementID);

        $title= $request->input("title");
        $desc= $request->input("description");
        $price= $request->input("price");
        $path= $request->input("image");
        $genre= $request->input("genres");

        $temp = [
            "id_publisher"=> 2,
            'title_game' => $title,
            'genre_game' => $genre,
            'desc_game' => $desc,
            'date_game' => date('Y-m-d'),
            'id_minimum_requirement' => $idCountMinimum,
            'id_recommended_requirement' => $idCountRecommended,
            'price_game' => $price,
            'image_path' => $path,
            'hero_image_path' => $path,
            'status_game' => 1
        ];

        $add= new GameModel($temp);
        $add->save();

        alert()->success('Added Game to the Library', 'Add Game successfully!');

        return back();
    }
}
