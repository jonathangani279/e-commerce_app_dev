<?php

namespace App\Http\Controllers;

use Laravel\Socialite\Facades\Socialite;

use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;
use App\Models\UserModel;

use Exception;

class SocialiteController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function redirectToGoogle()
    {
        return Socialite::driver('google')->redirect();
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function handleGoogleCallback()
    {
        try {
            $user = Socialite::driver('google')->stateless()->user();
            $findUser = UserModel::where('google_id', $user->id)->first();

            if ($findUser) {
                Auth::login($findUser, true);

                return redirect('/');
            } else {
                $newUser = UserModel::create([
                    'name_user' => $user->name,
                    'email_user' => $user->email,
                    'google_id' => $user->id,
                    'image_path' => $user->avatar,
                    'role_user' => 1,
                    'status_user'=> 1
                ]);

                Auth::login($newUser, true);

                return redirect('/');
            }

        } catch (Exception $e) {
            dd($e->getMessage());
        }
    }
}
