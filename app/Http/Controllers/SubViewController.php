<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SubViewController extends Controller
{
    public function products_list(Request $request) {
        $games= json_decode(json_encode($request->games));

        return view('pages.customer.products.list', ['games' => $games]);
    }

    public function libraries_list(Request $request) {
        $libraries= json_decode(json_encode($request->libraries));

        return view('pages.customer.libraries.list', ['libraries' => $libraries]);
    }

    public function libraries_detail(Request $request) {
        $library= json_decode(json_encode($request->library));

        return view('pages.customer.libraries.detail', ['library' => $library]);
    }

    public function profile_dashboard(Request $request) {
        return view('pages.customer.profile.'.$request->dashboard);
    }

    public function publisher_dashboard(Request $request) {
        return view('pages.publisher.'.$request->dashboard);
    }
}
