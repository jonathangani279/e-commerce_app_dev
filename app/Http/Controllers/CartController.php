<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
class CartController extends Controller
{
    //
    public function addToCart($id){
        if(Auth::check()){
            $data = [
                "id" => null,
                "id_user" => Auth::user()->id,
                "id_game" => $id
            ];
            DB::table('cart')->insert($data);
            alert()->success('Successfully saved!','Add To Cart Success');
            return back();
        }
        else{
            return view('pages.login');
        }
    }
}
