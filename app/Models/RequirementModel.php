<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RequirementModel extends Model
{
    use HasFactory;

    protected $fillable= [
        'id_requirement',
        'os_requirement',
        'cpu_requirement',
        'gpu_requirement',
        'ram_requirement',
        'storage_requirement'
    ];
    protected $table= 'requirements';
}
