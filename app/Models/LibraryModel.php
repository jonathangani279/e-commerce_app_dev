<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LibraryModel extends Model
{
    use HasFactory;

    protected $fillable= [
        'id_customer',
        'id_product'
    ];
    protected $table= 'libraries';

    public function customer() {
        return $this->belongsTo(UserModel::class, 'id_customer', 'id');
    }

    public function game() {
        return $this->hasOne(GameModel::class, 'id_game', 'id_product')->with([
            'publisher',
            'minimum_requirement',
            'recommended_requirement'
        ]);
    }
}
