<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Models\GameModel;

class UserModel extends Authenticatable
{
    use HasFactory;

    protected $primaryKey = 'id';

    protected $fillable= [
        'name_user',
        'email_user',
        'username_user',
        'password_user',
        'image_path',
        'role_user',
        'google_id',
        'status_user',
    ];
    protected $table= 'users';

    //RELATION INI BUAT CUSTOMER DOANG. PUBLISHER BEDA YAA
    public function libraries() {
        return $this->hasMany(GameModel::class, 'id_customer', 'id');
    }

    //RELATION INI BUAT PUBLISHER DOANG. LIBRARY BEDA YAA
    public function games() {
        return $this->hasMany(GameModel::class, 'id_publisher', 'id');
    }
}
