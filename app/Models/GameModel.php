<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GameModel extends Model
{
    use HasFactory;

    protected $fillable= [
        'id_publisher',
        'title_game',
        'genre_game',
        'desc_game',
        'date_game',
        'id_minimum_requirement',
        'id_recommended_requirement',
        'price_game',
        'image_path',
        'hero_image_path',
        'video_path',
        'status_game'
    ];
    protected $table= 'games';

    public function publisher() {
        return $this->belongsTo(UserModel::class, 'id_publisher', 'id');
    }

    public function minimum_requirement() {
        return $this->hasOne(RequirementModel::class, 'id_requirement', 'id_minimum_requirement');
    }

    public function recommended_requirement() {
        return $this->hasOne(RequirementModel::class, 'id_requirement', 'id_recommended_requirement');
    }
}
