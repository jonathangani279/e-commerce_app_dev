$(document).ready(function() {
    $("#modal").iziModal();

    $('#backTop').backTop({
        'position' : 5,
        'speed' : 500,
        'color' : 'red',
    });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
});
