<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

use App\Models\UserModel;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $seeds= [
            [
                'name_user' => 'Sapa Saya?',
                'email_user' => 'spsy@mail.com',
                'username_user' => 'spsy',
                'password_user' => Hash::make('spsy'),
                'image_path' => config('app.url').'/medias/images/icons/default-profile.svg',
                'role_user' => 1,
                'status_user' => 1,
            ],

            [
                'name_user' => 'Saya Publisher',
                'email_user' => 'publisher@mail.com',
                'username_user' => 'publisher',
                'password_user' => Hash::make('publisher'),
                'image_path' => config('app.url').'/medias/images/icons/default-profile.svg',
                'role_user' => 2,
                'status_user' => 1,
            ]
        ];

        foreach ($seeds as $key => $seed) {
            UserModel::create($seed);
        }
    }
}
