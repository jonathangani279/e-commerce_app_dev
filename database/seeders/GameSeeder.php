<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\GameModel;

class GameSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $seeds= [
            [
                'id_publisher' => 1,
                'title_game' => 'Game 1',
                'genre_game' => 'Horror',
                'desc_game' => 'Good Game',
                'date_game' => date('Y-m-d'),
                'id_minimum_requirement' => 1,
                'id_recommended_requirement' => 2,
                'price_game' => 1000,
                'url_path' => config('app.url').'/products/game-1',
                'status_game' => 1
            ],

            [
                'id_publisher' => 1,
                'title_game' => 'Game 2',
                'desc_game' => 'Good Game',
                'genre_game' => 'Adventure',
                'date_game' => date('Y-m-d'),
                'id_minimum_requirement' => 3,
                'id_recommended_requirement' => 4,
                'price_game' => 2000,
                'url_path' => config('app.url').'/products/game-2',
                'status_game' => 1
            ],

            [
                'id_publisher' => 1,
                'title_game' => 'Game 3',
                'genre_game' => 'Fighting',
                'desc_game' => 'Good Game',
                'date_game' => date('Y-m-d'),
                'id_minimum_requirement' => 5,
                'id_recommended_requirement' => 6,
                'price_game' => 3000,
                'url_path' => config('app.url').'/products/game-3',
                'status_game' => 1
            ],

            [
                'id_publisher' => 1,
                'title_game' => 'Game 4',
                'genre_game' => 'MMORPG',
                'desc_game' => 'Good Game',
                'date_game' => date('Y-m-d'),
                'id_minimum_requirement' => 7,
                'id_recommended_requirement' => 8,
                'price_game' => 3000,
                'url_path' => config('app.url').'/products/game-4',
                'status_game' => 1
            ],

            [
                'id_publisher' => 1,
                'title_game' => 'Game 5',
                'genre_game' => 'Puzzle',
                'desc_game' => 'Good Game',
                'date_game' => date('Y-m-d'),
                'id_minimum_requirement' => 9,
                'id_recommended_requirement' => 10,
                'price_game' => 3000,
                'url_path' => config('app.url').'/products/game-5',
                'status_game' => 1
            ],

            [
                'id_publisher' => 1,
                'title_game' => 'Game 6',
                'genre_game' => 'MMORPG',
                'desc_game' => 'Good Game',
                'date_game' => date('Y-m-d'),
                'id_minimum_requirement' => 11,
                'id_recommended_requirement' => 12,
                'price_game' => 3000,
                'url_path' => config('app.url').'/products/game-6',
                'status_game' => 1
            ],

            [
                'id_publisher' => 1,
                'title_game' => 'Game 7',
                'genre_game' => 'FPS Shooting',
                'desc_game' => 'Good Game',
                'date_game' => date('Y-m-d'),
                'id_minimum_requirement' => 13,
                'id_recommended_requirement' => 14,
                'price_game' => 3000,
                'url_path' => config('app.url').'/products/game-7',
                'status_game' => 1
            ],

            [
                'id_publisher' => 1,
                'title_game' => 'Game 8',
                'genre_game' => 'Rhythm',
                'desc_game' => 'Good Game',
                'date_game' => date('Y-m-d'),
                'id_minimum_requirement' => 15,
                'id_recommended_requirement' => 16,
                'price_game' => 3000,
                'url_path' => config('app.url').'/products/game-8',
                'status_game' => 1
            ],

            [
                'id_publisher' => 1,
                'title_game' => 'Game 9',
                'genre_game' => 'Visual Novel',
                'desc_game' => 'Good Game',
                'date_game' => date('Y-m-d'),
                'id_minimum_requirement' => 17,
                'id_recommended_requirement' => 18,
                'price_game' => 3000,
                'url_path' => config('app.url').'/products/game-9',
                'status_game' => 1
            ]
        ];

        foreach ($seeds as $key => $seed) {
            GameModel::create($seed);
        }
    }
}
