<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\LibraryModel;

class LibrarySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $seeds= [
            [
                'id_customer' => 1,
                'id_product' => 1,
            ],

            [
                'id_customer' => 1,
                'id_product' => 2,
            ],

            [
                'id_customer' => 1,
                'id_product' => 3,
            ],

            [
                'id_customer' => 1,
                'id_product' => 4,
            ],

            [
                'id_customer' => 1,
                'id_product' => 5,
            ]
        ];

        foreach ($seeds as $key => $seed) {
            LibraryModel::create($seed);
        }
    }
}
