<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\RequirementModel;

class RequirementSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $seeds= [
            [
                'os_requirement' => '64-bit Windows 7 or 64-bit Windows 10. DirectX® 12',
                'cpu_requirement' => 'Intel Core i5-3570K or AMD FX-8310',
                'gpu_requirement' => 'Nvidia GeForce GTX 780 3GB or AMD Radeon RX 470',
                'ram_requirement' => '8 GB',
                'storage_requirement' => '70 GB HDD (SSD recommended)'
            ],

            [
                'os_requirement' => '64-bit Windows 10. DirectX 12',
                'cpu_requirement' => 'Intel Core i7-4790 or AMD Ryzen 3 3200G',
                'gpu_requirement' => 'NVIDIA GeForce GTX 1060 6 GB or AMD Radeon R9 Fury',
                'ram_requirement' => '12 GB',
                'storage_requirement' => '70 GB SSD'
            ]
        ];

        foreach ($seeds as $key => $seed) {
            RequirementModel::create($seed);
        }
    }
}
