<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('games', function (Blueprint $table) {
            $table->bigIncrements('id_game');
            $table->integer('id_publisher');
            $table->string('title_game');
            $table->string('genre_game');
            $table->string('desc_game');
            $table->date('date_game');
            $table->integer('id_minimum_requirement');
            $table->integer('id_recommended_requirement');
            $table->integer('price_game');
            $table->string('hero_image_path')->nullable();
            $table->string('image_path')->nullable();
            $table->string('video_path')->nullable();
            $table->string('url_path')->nullable();
            $table->tinyInteger('status_game');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('games');
    }
}
